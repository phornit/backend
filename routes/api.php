<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Auth
Route::apiResources(['secUsers' => 'Auth\SecUserController']);
Route::apiResources(['role' => 'Auth\RoleController']);
Route::post('authenticate', 'Auth\LoginController@authenticate');
Route::get('getCurrentUser', 'Auth\SecUserController@getCurrentUser');

Route::post('userRegister', 'Auth\RegisterController@UserRegister');
