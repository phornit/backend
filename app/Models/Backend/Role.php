<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = "core_role";
    protected $fillable = [
        'name',
        'description',
    ];
}
