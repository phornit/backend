<?php


namespace App\Libraries;

use App\Enums\EnumsConfig;

class CoreFunction
{
    static function Pagination(){
        $num = EnumsConfig::BPNumList;
        $paginate = (!empty(\Request::get('paginate'))) ? \Request::get('paginate') : $num;
        return $paginate;
    }
}
