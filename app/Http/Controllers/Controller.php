<?php

namespace App\Http\Controllers;

use App\Http\Resources\sendCollection;
use App\Http\Resources\sendResource;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendListResponse($data, $message = 'Success', $statusCode = 200)
    {
        return new sendCollection($data, $message, $statusCode);
    }

    public function sendResponse($data, $statusCode = 200 , $message = 'Success'){
        return new sendResource($data, $message, $statusCode);
    }

    public function sendNotFoundResponse($message = 'Record Not Found')
    {
        $response = [
            'data'    =>  null ,
            'statusCode' => 404,
            'message' => $message,
        ];
        return response()->json($response, 404);
    }

    public function sendInvalidResponse($message)
    {
        $msg = '';
        foreach( $message->all() as $value) {
            $msg .= $value."<br/>";
        }

        $response = [
            'data'    =>  null ,
            'statusCode' => 404,
            'message' => $msg,
        ];
        return response()->json($response, 400);
    }

    public function sendError($error, $errorMessages = [], $code = 404){

        $response = [
            'data' => null,
            'success' => false,
            'statusCode' => 404,
            'message' => $error,
        ];

        if(!empty($errorMessages)){
            $response['error_messages'] = $errorMessages;
        }
        return response()->json($response, $code);
    }

    public function sendInvalidApiKeyResponse($message = 'Invalid API Key')
    {
        $response = [
            'data'    =>  null ,
            'statusCode' => 404,
            'message' => $message,
        ];
        return response()->json($response, 401);
    }

}
